package DBI_API_TEST.DBI_API_TEST;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.io.IOUtils;
import org.json.simple.ItemList;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.BasicSessionCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;

import io.restassured.http.ContentType;
import io.restassured.http.Method;
import io.restassured.internal.support.FileReader;
import io.restassured.path.json.JsonPath;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.response.ValidatableResponseOptions;
import io.restassured.specification.RequestSpecification;
//import io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.QueryOutcome;
import com.amazonaws.services.dynamodbv2.document.ScanOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.GetItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec;
import com.amazonaws.services.dynamodbv2.document.spec.ScanSpec;
import com.amazonaws.services.dynamodbv2.document.utils.NameMap;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;


public class TC001_DBI_Post_Order_RequestTest {

public static String jsonAsString;

// Test

	@Test
	 
	 
	public  static void TC001_CreateOrder() throws IOException 
	 {
		 
		
		// DBI TOKEN CREDENTIALS
		
		
		FileInputStream fileinputstream = new FileInputStream(new File("DBI_JsonFile_Input\\DbiTokenCredentials.properties"));
		
	    Properties prop = new Properties();
			    	 
		prop.load(fileinputstream);
			    	 
			    	 
			    	 
		String username = prop.getProperty("username");
			    	 


	    String password = prop.getProperty("password");
			    	 
			    	 

		String bearertoken = prop.getProperty("bearertoken");
			    	 
		

		String xappid = prop.getProperty("x-appid");
		
			    
	  		 
		
		//  AUTO GENERATE TOKEN --  GET TOKEN
		
		
		  Response response =RestAssured.given()
				  
				  .header("username", username)
				  .header("password", password)
				  .header("bearertoken", bearertoken)
				  .header("x-appid", xappid)
				  .when()
				  .get("https://stage-auth.dbiaws.com/v1")
				  .then().contentType(ContentType.JSON).extract().response();
				
		  
		  jsonAsString = response.asString();
				  
		  
		  System.out.println(jsonAsString);
		  
		  JsonPath extractor = response.jsonPath();
		  
		  String token = extractor.get("token");
		  
		  // Test
		  
		  System.out.println(token);
				 
						  response
					      .then()
					      .log()
					      .body()
					      .statusCode(200);		
									
						 
							
			
			// TH ORDER DATA FILE LOCATION

			FileInputStream fileinputstream1 = new FileInputStream(new File(".\\DBI_JsonFile_Input\\OrderData.json"));
				 
				 
			 
			  //Specify base URI
			  RestAssured.baseURI="https://stage-transaction.dbiaws.com/v1";
			  
			  //Request object
			  RequestSpecification httpRequest=RestAssured.given()
			  
			  
			 
			  // Headers
			  .header("Content-Type","application/json")
			  .header("Authorization","Bearer" + token)
			  .header("authorizationtoken", token)
			  .and()
			  .body(IOUtils.toString(fileinputstream1, "UTF-8"));
			  
			  
			  
			  //Response object
			  Response response1=httpRequest.request(Method.POST, "/order");
			   
			  
			  // Extract TransactionUuid from "GET" Body response -- Needed for DynamoDB Validation
			  
		     JsonPath extractor1 = response1.jsonPath();
			  
		     String uuid = extractor1.get("transactionUuid");
		 	  
			  
		     // Print Transaction Uuid
			  System.out.println(uuid);
			 
			  
			  
			 		  
			  //print response in console window
			  
			  String responseBody1=response1.getBody().asString();
			  System.out.println("Response Body is:" +responseBody1);
			  
			 
			  
			   httpRequest.then();

		
			   // Assert ResponseBody
			   
			  Assert.assertNotNull(responseBody1);
			  
			  
			  try
			  {
			  Assert.assertEquals(responseBody1.contains("TESTNG_CM"), true);
        	  Assert.assertEquals(responseBody1.contains("externalSystemId1"), true);
			  Assert.assertEquals(responseBody1.contains("taxRemitted"), true);
			  }
			  
			  catch (Exception e)
			  
			  {
				  
				  System.out.println("Expected Values from body not found in RESPONSE BODY");
				  
			  }
			  
			  
			  //Status code validation
			  int statusCode1=response1.getStatusCode();
			  System.out.println("Status code is: "+statusCode1);
			  Assert.assertEquals(statusCode1, 201);
			  
			  System.out.println(uuid);
				 		  
			  // success code validation
			  
			  String successCode1=response1.jsonPath().get("SuccessCode");
			  
	 
			 // Assert.assertEquals(successCode, "OPERATION_SUCCESS");
			  

			  
			  
 // DYNAMODB -- Find newly create transaction and validate Data
			  

			  
			
			// AWS Credentials Access/Location
			  
			FileInputStream fileinputstream2 = new FileInputStream(new File("DBI_JsonFile_Input\\AwsCredentials.properties"));
				    					
		    Properties prop2 = new Properties();
				    	 
			prop.load(fileinputstream2);
				    	 
				    	 
				    	 
			String aws_access_key_id = prop.getProperty("aws_access_key_id");
				    	 


		    String aws_secret_access_key = prop.getProperty("aws_secret_access_key");
				    	 
				    	 

			String aws_session_token = prop.getProperty("aws_session_token");
				    	 
				    	
				    
		   BasicSessionCredentials sessionCredentials = new 
				   
				   
		   BasicSessionCredentials(aws_access_key_id, aws_secret_access_key, aws_session_token );

				    	
				    		
		   
		  // Set Region
		   
		  AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().withRegion(Regions.US_EAST_1).withCredentials(new AWSStaticCredentialsProvider(sessionCredentials)).build();
		  DynamoDB dynamoDB = new DynamoDB(client);
		  
		  
		  // Find Table
		  String tableName = "transactionTransaction";
		  Table table = dynamoDB.getTable(tableName);
				        
		  DynamoDBMapper mapper = new DynamoDBMapper(client);
	      Map<String, Object> expressionAttributeValues = new HashMap<String, Object>();
				           
				      
				        
		  ScanRequest scanRequest = new ScanRequest()
				  .withTableName("transactionTransaction");
				    	

				    	
			// Find Transaction in DynamoDB using transactionuuid
				    	
		  	ScanResult result = client.scan(scanRequest);
					     	
				Item item;
					     	
					     	
				item = table.getItem("transactionUuid", uuid);
					     	
				
				
				// Assert that Table is populated
				Assert.assertNotNull(item);
				
				// Display full transaction in table
				
				System.out.println(item);
				
									     	
				// QUERY Table using UUID and display data as Json
					     	
					     	
					 	
				QuerySpec spec8 = new QuerySpec() 
					    .withKeyConditionExpression("transactionUuid = :nn") 
					    .withValueMap(new ValueMap() 
					    .withString(":nn", "shippingamount"));
					     		   
					     ItemCollection<QueryOutcome> items = table.query(spec8);  
					     Iterator<Item> iterator = items.iterator(); 
					     Item item8 = null; 

					     while (iterator.hasNext()) { 
					     		item8 = iterator.next(); 
					     		System.out.println("Shipping Amount is " + item8.toJSONPretty());
					     		   }
					     					    					     	

					     	// ITEM SPECIFIC Transaction 
					     	
					     	GetItemSpec spec1 = new GetItemSpec() 
					     		   .withPrimaryKey("transactionUuid", uuid) 
					     		   //.wi
					     		   //.withProjectionExpression("messageDate, messageId") 
					     		   //.withProjectionExpression("messageDate") 
					     		   .withConsistentRead(true);

					     		Item item4 = table.getItem(spec1);
					     	
					     		
					     		
					     		System.out.println(item4.toJSONPretty());
					     	
					     	
					     					     	
					     	
					     	// ITEM SPECIFIC Transaction Metadata
					     	
					     	GetItemSpec spec = new GetItemSpec() 
					     		   .withPrimaryKey("transactionUuid", uuid) 
					     		   .withProjectionExpression("transactionMetadata, messageDate, createdBy") 
					     		   //.withProjectionExpression("messageDate") 
					     		   .withConsistentRead(true);

					     		Item item3 = table.getItem(spec);
					     	
					     		
					     		
					     		System.out.println("Transaction Metadata is " + item3.toJSONPretty());
					     	
					     		
					     
							
							  
					
					     	//System.out.println(item);
					     	
					     	
					    // 	for (Map<String, AttributeValue> item : result.getItems()){
					     		
					  
					     // System.out.println(item);
				    		
				    		
				    		
				    		
				    		
				    		
				    		
				    		
				    		
				    		
				    		
				    		
				    		
				    		
				    		// TRY THIS
				    		
				    		
				    		
				    		
				    		
				    		
				    		
				 
				    		// System.out.println(item);
				    		 
				    		 
				    		 
				    		 
				      
				    /*
				    	 Table table = dynamoDB.getTable(tableName);
				         Map<String, Object> expressionAttributeValues = new HashMap<String, Object>();
				            
				         expressionAttributeValues.put(":lastName", "firstName");
				         
				     	ScanRequest scanRequest = new ScanRequest()
				     	    .withTableName("Order");
				     		
				     	
				     	
				     	 ScanSpec scanSpec = new ScanSpec().withProjectionExpression("LastName, FirstName")
				                 .withFilterExpression("Carl").withNameMap(new NameMap().with("#carl", "b"));
				               //  .withValueMap(new ValueMap().withNumber(":start_yr", 1950).withNumber(":end_yr", 1959));

				     	try {
				            ItemCollection<ScanOutcome> items = table.scan(scanSpec);

				            Iterator<Item> iter = items.iterator();
				            while (iter.hasNext()) {
				                Item item = iter.next();
				                System.out.println(item.toString());
				            }

				        }
				        catch (Exception e) {
				            System.err.println("Unable to scan the table:");
				            System.err.println(e.getMessage());
				        }

				     	ScanResult result = client.scan(scanRequest);
				     	
				     	Item item;
				     	
				     	
				     	item = table.getItem("LastName", "James");
				     	
				
				     	System.out.println(item);
				     	
				     	
				     	for (Map<String, AttributeValue> item : result.getItems()){
				     		
				  
				      System.out.println(item);
				      
				      
				      
				      
				     		 */
				     		 
				     		 
				     		 
				     		 
				    	}





	  
	  
	 
	  
	 }
				    

	
	
	
	
	

